package com.marcantoine.librairy_management.repositories;

import com.marcantoine.librairy_management.exception.UserNotFoundException;
import com.marcantoine.librairy_management.models.Rent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRentRepository extends JpaRepository<Rent, Long> {
    default List<Rent> findByUserId(Long userId) {
        return this.findAll().stream().filter(rent -> rent.getUser().getId().equals(userId)).toList();
    }
}
