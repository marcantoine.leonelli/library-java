package com.marcantoine.librairy_management.repositories;

import com.marcantoine.librairy_management.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBookRepository extends JpaRepository<Book, Long> {
}
