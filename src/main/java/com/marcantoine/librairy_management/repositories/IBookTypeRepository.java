package com.marcantoine.librairy_management.repositories;

import com.marcantoine.librairy_management.models.BookType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBookTypeRepository extends JpaRepository<BookType, Long> {
}
