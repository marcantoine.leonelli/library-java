package com.marcantoine.librairy_management.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "dbuser")
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username;
    /**
     * j'ai essayer de crypter le mot de passe mais ça nécessité de remettre
     * spring security et donc ça cryptait toutes les routes
     */
    private String password;
    private String email;
    private String role;
}
