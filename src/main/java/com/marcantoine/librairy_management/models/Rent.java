package com.marcantoine.librairy_management.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Entity
@Table(name = "rent")
@Getter
@Setter
@ToString
public class Rent {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private Book book;

    @ManyToOne
    private User user;

    private Date rentDate;
    private Date returnDate;
}
