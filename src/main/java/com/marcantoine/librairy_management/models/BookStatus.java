package com.marcantoine.librairy_management.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

public enum BookStatus {
    AVAILABLE,
    RENTED,
    LOST
}
