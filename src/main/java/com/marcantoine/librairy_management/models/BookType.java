package com.marcantoine.librairy_management.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book_type")
@Entity
public class BookType {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Author author;

    private String title;
    private Date datePublication;
}
