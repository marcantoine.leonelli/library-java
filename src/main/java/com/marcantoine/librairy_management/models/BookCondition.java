package com.marcantoine.librairy_management.models;

public enum BookCondition {
    NEW,
    GOOD,
    BAD
}
