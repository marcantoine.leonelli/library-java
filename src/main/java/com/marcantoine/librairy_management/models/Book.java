package com.marcantoine.librairy_management.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Set;

/**
 * Book représente un livre dans la bibliothèque.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book")
@Entity
public class Book {
    @Id
    @GeneratedValue
    private Long id;

    /**
     * AVAILABLE ou RENTED ou LOST
     */
    private BookStatus status;
    /**
     * L'état physique du livre
     */
    private BookCondition condition;

    /**
     * Le type du livre, j'aurai pu faire une relation d'héritage mais
     * j'ai préféré faire une relation de composition pour simplifier et mieux coller à la représentation en db
     */
    @ManyToOne
    private BookType type;
}
