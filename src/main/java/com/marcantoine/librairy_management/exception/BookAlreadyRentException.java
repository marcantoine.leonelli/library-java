package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;

public class BookAlreadyRentException extends BaseException {
    public BookAlreadyRentException() {
        super("Book already rented");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage(this.getMessage(), HttpStatus.CONFLICT);
    }
}
