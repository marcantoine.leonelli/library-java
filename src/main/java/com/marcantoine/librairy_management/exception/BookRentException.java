package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BookRentException extends BaseException {
    public BookRentException(String message) {
        super(message);
    }

    public BookRentException() {
        super("Error while renting book");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage(this.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
