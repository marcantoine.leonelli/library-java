package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UserNotFoundException extends BaseException {
    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException() {
        super("User not found");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage(this.getMessage(), HttpStatus.NOT_FOUND);
    }
}
