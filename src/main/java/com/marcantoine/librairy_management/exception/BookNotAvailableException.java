package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;

public class BookNotAvailableException extends BaseException {
    public BookNotAvailableException(String message) {
        super(message);
    }

    public BookNotAvailableException() {
        super("Book is not available");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage(this.getMessage(), HttpStatus.NOT_FOUND);
    }
}