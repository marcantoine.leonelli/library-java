package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;

public class RentNotFoundException extends BaseException {
    public RentNotFoundException() {
        super("book not found");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage("Rent not found", HttpStatus.NOT_FOUND);
    }
}
