package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BookNotFoundException extends BaseException {
    public BookNotFoundException(String message) {
        super(message);
    }

    public BookNotFoundException() {
        super("Book not found");
    }

    @Override
    public ExceptionMessage getExceptionMessage() {
        return new ExceptionMessage(this.getMessage(), HttpStatus.NOT_FOUND);
    }
}
