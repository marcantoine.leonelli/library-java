package com.marcantoine.librairy_management.exception;

import com.marcantoine.librairy_management.dto.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseException extends Exception {
    public BaseException(String message) {
        super(message);
    }

    public BaseException() {
        super("Error");
    }

    /**
     * pour avoir un message code d'erreur plus précis quand je throw une erreur
     * @return
     */
    public abstract ExceptionMessage getExceptionMessage();
}
