package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.models.User;
import com.marcantoine.librairy_management.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@AllArgsConstructor
@Service
public class UserServiceImpl implements IUserService {
    private final IUserRepository userRepo;

    @Override
    public User addUser(User user) {
        User save = userRepo.save(user);
        return save;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }
}
