package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.models.User;

import java.util.List;

public interface IUserService {
    public User addUser(User user);

    public List<User> getAllUsers();
}
