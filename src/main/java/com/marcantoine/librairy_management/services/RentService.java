package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.exception.*;
import com.marcantoine.librairy_management.models.Book;
import com.marcantoine.librairy_management.models.BookStatus;
import com.marcantoine.librairy_management.models.Rent;
import com.marcantoine.librairy_management.models.User;
import com.marcantoine.librairy_management.repositories.IBookRepository;
import com.marcantoine.librairy_management.repositories.IRentRepository;
import com.marcantoine.librairy_management.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class RentService {
    private final IBookRepository bookRepo;
    private final IUserRepository userRepo;
    private final IRentRepository rentRepo;

    public Rent rentBook(Long bookId, Long userId) throws BaseException {
        Book book = bookRepo.findById(bookId).orElseThrow(() -> new BookNotFoundException());
        User user = userRepo.findById(userId).orElseThrow(() -> new UserNotFoundException());

        if (book.getStatus() == BookStatus.AVAILABLE) {
            Rent rent = new Rent();
            rent.setBook(book);
            rent.setUser(user);
            rent.setRentDate(new Date());
            book.setStatus(BookStatus.RENTED);
            rentRepo.save(rent);
            bookRepo.save(book);

            return rent;
        } else {
            throw new BookNotAvailableException();
        }
    }

    public List<Rent> getAllRents() {
        return rentRepo.findAll();
    }

    public void returnBook(Long rentId) throws BaseException {
        Rent rent = rentRepo.findById(rentId).orElseThrow(() -> new RentNotFoundException());
        Book book = rent.getBook();
        book.setStatus(BookStatus.AVAILABLE);
        rent.setReturnDate(new Date());
        rentRepo.save(rent);
        bookRepo.save(book);
    }

    public List<Rent> getRentsByUserId(Long userId) {
        return rentRepo.findByUserId(userId);
    }
}
