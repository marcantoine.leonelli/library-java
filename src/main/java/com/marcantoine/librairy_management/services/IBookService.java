package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.dto.RentDTO;
import com.marcantoine.librairy_management.exception.BaseException;
import com.marcantoine.librairy_management.models.Book;

import java.util.List;

public interface IBookService {
    public Book addBook(Book book);

    public List<Book> getAllBooks();
}
