package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.dto.RentDTO;
import com.marcantoine.librairy_management.exception.BaseException;
import com.marcantoine.librairy_management.models.Book;
import com.marcantoine.librairy_management.repositories.IBookRepository;
import com.marcantoine.librairy_management.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class BookServiceImpl implements IBookService {
    private final IBookRepository bookRepo;

    @Override
    public Book addBook(Book book) {
        return bookRepo.save(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepo.findAll();
    }
}
