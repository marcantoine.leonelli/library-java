package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.models.Author;
import com.marcantoine.librairy_management.repositories.IAuthorRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthorService {
    private final IAuthorRepository authorRepo;

    public Author createAuthor(Author author){
        return authorRepo.save(author);
    }

    public Author getAuthorById(Long id){
        return authorRepo.findById(id).orElse(null);
    }

    public Author updateAuthor(Author author){
        Author existingAuthor = authorRepo.findById(author.getId()).orElse(null);
        existingAuthor.setFirstName(author.getFirstName());
        existingAuthor.setLastName(author.getLastName());
        return authorRepo.save(existingAuthor);
    }

    public String deleteAuthor(Long id){
        authorRepo.deleteById(id);
        return "Author " + id + " has been deleted";
    }

    public Iterable<Author> getAllAuthors(){
        return authorRepo.findAll();
    }
}
