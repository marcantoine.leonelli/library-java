package com.marcantoine.librairy_management.services;

import com.marcantoine.librairy_management.models.BookType;
import com.marcantoine.librairy_management.repositories.IBookTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BookTypeService {
    private final IBookTypeRepository bookTypeRepo;

    public BookType createBookType(BookType bookType){
        return bookTypeRepo.save(bookType);
    }

    public BookType getBookTypeById(Long id){
        return bookTypeRepo.findById(id).orElse(null);
    }

    public List<BookType> getAllBookTypes() {
        return (List)bookTypeRepo.findAll();
    }

    public BookType updateBookType(BookType bookType){
        BookType existingBookType = bookTypeRepo.findById(bookType.getId()).orElse(null);
        existingBookType.setAuthor(bookType.getAuthor());
        existingBookType.setTitle(bookType.getTitle());
        return bookTypeRepo.save(existingBookType);
    }

    public String deleteBookType(Long id){
        bookTypeRepo.deleteById(id);
        return "BookType " + id + " has been deleted";
    }
}
