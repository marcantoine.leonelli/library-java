package com.marcantoine.librairy_management.controllers;

import com.marcantoine.librairy_management.dto.BookDTO;
import com.marcantoine.librairy_management.dto.RentDTO;
import com.marcantoine.librairy_management.exception.BaseException;
import com.marcantoine.librairy_management.models.Book;
import com.marcantoine.librairy_management.models.BookStatus;
import com.marcantoine.librairy_management.models.BookType;
import com.marcantoine.librairy_management.services.BookTypeService;
import com.marcantoine.librairy_management.services.IBookService;
import com.marcantoine.librairy_management.services.IUserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class BookController {
    private final IBookService bookService;
    private final BookTypeService bookTypeService;

    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @PostMapping("/book")
    public ResponseEntity<Book> addBook(@RequestBody BookDTO bookDTO) {
        if (bookDTO.getCondition() == null || bookDTO.getTypeId() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Book book = new Book();
        BookType bookType = bookTypeService.getBookTypeById(bookDTO.getTypeId());
        book.setCondition(bookDTO.getCondition());
        book.setStatus(BookStatus.AVAILABLE);
        book.setType(bookType);

        Book newBook = bookService.addBook(book);

        return new ResponseEntity<>(newBook, HttpStatus.CREATED);
    }

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getAllBooks() {
        return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
    }
}
