package com.marcantoine.librairy_management.controllers;

import com.marcantoine.librairy_management.dto.RentDTO;
import com.marcantoine.librairy_management.exception.BaseException;
import com.marcantoine.librairy_management.models.Book;
import com.marcantoine.librairy_management.models.Rent;
import com.marcantoine.librairy_management.services.RentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
public class RentController {
    private final RentService rentService;

    @GetMapping("/rents")
    public List<Rent> getRents() {
        return rentService.getAllRents();
    }

    @GetMapping("/rents/{userId}")
    public ResponseEntity<Object> getRentsByUserId(@PathVariable Long userId) {
        List<Rent> rents = rentService.getRentsByUserId(userId);
        return new ResponseEntity<>(rents, HttpStatus.OK);
    }

    @PostMapping("/rent")
    public ResponseEntity<Object> rentBook(@RequestBody RentDTO rentDTO) {
        try {
            Rent rentedBook = rentService.rentBook(rentDTO.getBookId(), rentDTO.getUserId());
            return new ResponseEntity<>(rentedBook, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity<>(e.getExceptionMessage().getMessage(), e.getExceptionMessage().getStatus());
        }
    }

    @GetMapping("/rent/return/{rentId}")
    public ResponseEntity<Object> returnBook(@PathVariable int rentId) {
        try {
            rentService.returnBook((long)rentId);
            return new ResponseEntity<>("Book returned", HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity<>(e.getExceptionMessage().getMessage(), e.getExceptionMessage().getStatus());
        }
    }
}
