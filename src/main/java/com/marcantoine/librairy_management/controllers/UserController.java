package com.marcantoine.librairy_management.controllers;

import com.marcantoine.librairy_management.dto.UserDTO;
import com.marcantoine.librairy_management.models.User;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.marcantoine.librairy_management.services.IUserService;

import java.util.List;

@AllArgsConstructor
@RestController
public class UserController {
    private IUserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity<UserDTO> addUser(@RequestBody User user) {
        User newUser = userService.addUser(user);
        UserDTO userDTO = new UserDTO(newUser);
        return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
    }
}
