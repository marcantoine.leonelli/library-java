package com.marcantoine.librairy_management.controllers;

import com.marcantoine.librairy_management.dto.BookTypeDTO;
import com.marcantoine.librairy_management.models.Author;
import com.marcantoine.librairy_management.models.BookType;
import com.marcantoine.librairy_management.services.AuthorService;
import com.marcantoine.librairy_management.services.BookTypeService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
public class BookTypeController {
    private final BookTypeService bookTypeService;
    private final AuthorService authorService;

    private static final Logger log = LoggerFactory.getLogger(BookController.class);


    @GetMapping("/booktypes")
    public ResponseEntity<List<BookType>> getAllBookTypes() {
        return new ResponseEntity<>(bookTypeService.getAllBookTypes(), HttpStatus.OK);
    }

    @GetMapping("/booktype/{id}")
    public ResponseEntity<BookType> getBookTypeById(@PathVariable Long id) {
        return new ResponseEntity<>(bookTypeService.getBookTypeById(id), HttpStatus.OK);
    }

    @PostMapping("/booktype")
    public ResponseEntity<BookType> createBookType(@RequestBody BookTypeDTO bookTypeDTO) {
        if (bookTypeDTO.getAuthorId() == null || bookTypeDTO.getTitle() == null) {
            log.info("BookType creation failed: " + bookTypeDTO.toString());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        BookType bookType = new BookType();
        Author author = authorService.getAuthorById(bookTypeDTO.getAuthorId());
        bookType.setAuthor(author);
        bookType.setTitle(bookTypeDTO.getTitle());
        if (bookTypeDTO.getDatePublication() != null) {
            log.info("BookType creation with date: " + bookTypeDTO.getDatePublication());
            bookType.setDatePublication(Date.from(Instant.parse(bookTypeDTO.getDatePublication())));
        }

        return new ResponseEntity<>(bookTypeService.createBookType(bookType), HttpStatus.CREATED);
    }

    @DeleteMapping("/booktype/{id}")
    public ResponseEntity<String> deleteBookType(@PathVariable Long id) {
        return new ResponseEntity<>(bookTypeService.deleteBookType(id), HttpStatus.OK);
    }
}
