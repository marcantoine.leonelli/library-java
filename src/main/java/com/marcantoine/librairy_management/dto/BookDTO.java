package com.marcantoine.librairy_management.dto;

import com.marcantoine.librairy_management.models.BookCondition;
import com.marcantoine.librairy_management.models.BookStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookDTO {
    private Long id;
    private Long typeId;
    private BookCondition condition;
}
