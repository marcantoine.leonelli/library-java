package com.marcantoine.librairy_management.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@Setter
public class ExceptionMessage {
    private String message;
    private HttpStatus status;
}
