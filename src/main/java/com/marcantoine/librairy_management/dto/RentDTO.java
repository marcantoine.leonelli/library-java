package com.marcantoine.librairy_management.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RentDTO {
    private Long bookId;
    private Long userId;
}
