package com.marcantoine.librairy_management.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookTypeDTO {
    private Long authorId;
    private String title;
    private String datePublication;
}
